var db = require('./db');
var model = require('./model');

const invoke = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  const method = event.httpMethod;
  const path = event.path;

  db.connect(function (err, db) {
    if (err) throw err;
    if (method === 'GET') {
      if (path === '/currenttempincovilha') {
        model.getTemperatureAndTimeStamp(function (result) {
          callback(null, result);
        });
      }
      else if (path === '/avgtempinsfax') {
        model.getAverageTemperature(function (result) {
          callback(null, result);
        });
      }
      else {
        callback({ statusCode: 404, body: 'Please enter valid URL' });
      }
    }
  });
};

exports.invoker = invoke;