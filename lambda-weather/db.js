const MongoClient = require('mongodb').MongoClient;
const MONGODB_URI = "mongodb+srv://admin:admin@temperature.dolag.mongodb.net/weather_report?retryWrites=true&w=majority";

const state = {
  db: null
}

exports.connect = function (done) {
  if (state.db) return done();
  MongoClient.connect(MONGODB_URI, function (err, client) {
    if (err) return done(err);
    state.db = client.db();
    done();
  });
}

exports.get = function () {
  return state.db;
}

exports.close = function (done) {
  if (state.db) {
    state.db.close(function (err, result) {
      state.db = null
      state.mode = null
      done(err);
    })
  }
}