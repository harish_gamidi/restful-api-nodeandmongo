var db = require('./db');

exports.getTemperatureAndTimeStamp = function (callback) {
  db.get().collection('temperature').find({ city: 'Covilha' }).sort({ _id: -1 }).limit(1).toArray()
    .then((data) => {
      callback({ statusCode: 200, body: JSON.stringify(data) });
    })
    .catch(err => {
      callback({ statusCode: 500, body: err });
    });
}

exports.getAverageTemperature = function (callback) {
  db.get().collection('temperature').aggregate(
    [
      {
        $match: {
          month: "June",
          city: "Sfax"
        }
      },
      {
        $group: {
          _id: "$city",
          avgTemp: {
            $avg: "$temperature"
          },
          country: { $first: "$country" }
        }
      },
      { $sort: { avgTemp: -1 } },
      {
        $project: {
          "country": 1, "avgTemp": 1
        }
      }
    ]).toArray()
    .then((data) => {
      callback({ statusCode: 200, body: JSON.stringify(data) });
    })
    .catch(err => {
      callback({ statusCode: 500, body: err });
    });
}