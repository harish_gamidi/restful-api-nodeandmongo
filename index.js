const index = require('./lambda-weather/invoker');

exports.handler = (event, context, callback) => {
  index.invoker(event, context, callback);
};